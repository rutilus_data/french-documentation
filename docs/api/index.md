# Description de l'API Rest Rutilus Data

Bienvenue dans la documentation de notre API de plateforme DATA. Cette API offre trois fonctionnalités clés :

1. **Enregistrement de Données** : Vous pouvez facilement créer, mettre à jour, lire et supprimer des enregistrements 
de données via des requêtes HTTP.

2. **Ajout de Devices** : Gérez vos périphériques, définissez leurs propriétés et associez-les à vos données.

3. **JWT Authentification** : Sécurisez vos interactions avec notre API à l'aide de JSON Web Tokens, garantissant un accès 
sécurisé aux utilisateurs autorisés.

4. **Envoi de Données avec un token** : Notre dernière fonctionnalité vous permet d'envoyer facilement des 
données en utilisant un token associé à un appareil. Cela simplifie le processus d'envoi de données, en garantissant 
que seuls les appareils autorisés peuvent soumettre des informations à votre plateforme.

Cette documentation vous guidera à travers chaque aspect de l'API, avec des exemples de requêtes et des instructions 
claires pour une utilisation efficace. Si vous avez des questions, notre équipe de support est à votre disposition 
pour vous aider. Commencez dès maintenant à explorer les fonctionnalités de notre API pour une gestion avancée de vos données.