# Time séries : enregistrement et filtre des données

1. Requête et filtre des données d'un device
2. Enregistrement de données pour un device


## GET /api/timeseries/{device_id}/

### Path parameters

| Nom       |                                                                                               Description                                                                                               | Requis |
|-----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|-------:|
| device_id | L'ID de l'appareil, représenté par le paramètre de chemin (path parameter) {device_id}, est  l'identifiant unique qui permet de spécifier l'appareil pour lequel vous souhaitez effectuer des opérations |    Oui |


### Exemples de réponses

=== "200 Success"

    ```json
    [
        {
            "value": 192,
            "device_id": 2,
            "timestamp": "2023-09-28T18:46:20.587337Z"
        },
        {
            "value": 197,
            "device_id": 2,
            "timestamp": "2023-09-28T18:50:09.045879Z"
        }
    ]
    ```


=== "400 Bad Request"

    ```json
    {
        "detail": "Invalid device",
        "status_code": 400,
        "message": "Device does not exist or does not belong to you"
    }
    ```


## POST /api/timeseries/

Endpoint vous permettant d'enregistrer de nouvelles données associées à un device.

### Champs du corps de la requête

| Nom       |  Type  |                                                                  Description                                                                   |
|-----------|:------:|:----------------------------------------------------------------------------------------------------------------------------------------------:|
| value | float  |                                                     La valeur de la donnée à enregistrer.                                                      |
| token | string | Le token d'authentification associé au device. Dans la pratique, chaque device possède son propre jeton. Il est disponible via l'interface web |


### Champs du corps de la réponse

Lorsqu'une requête POST est effectuée pour ajouter une valeur à un appareil, le serveur renvoie une réponse pour confirmer que l'opération a été effectuée avec succès. 
Voici une explication des éléments clés de cette réponse :

| Nom       |  Type  |                                        Description                                        |
|-----------|:------:|:-----------------------------------------------------------------------------------------:|
| value | float  | Ce champ représente la valeur de la mesure ou de la donnée qui a été ajoutée avec succès. |
| device_id | integer |    Ce champ représente l'identifiant unique du device auquel la donnée a été ajoutée.     |
| timestamp | string | Ce champ représente la date et l'heure auxquelles la donnée a été ajoutée avec succès.    |

### Exemples de réponses

=== "201 Success"

    ```json
    {
        "value": 192,
        "device_id": 2,
        "timestamp": "2023-09-28T18:46:20.587337Z"
    }
    ```


=== "401 Unauthorized"

    ```json
    {
        "detail": "Authentication credentials were not provided.",
        "status_code": 401,
        "message": "Something went wrong"
    }
    ```