# Gestion des devices

## GET /api/devices/

Retourne la liste des devices associés à l'utilisateur.

### Authentification

Bearer Token (JWT), que vous pouvez obtenir en vous authentifiant avec votre compte adresse email et votre admin 
token disponible dans l'interface web. 

Pour obtenir plus d'information, voir la page : [Authentification](/docs/api/authentication.md)

### Exemples de réponses

=== "200 OK"

    ```json
        [
            {
                "id": 2,
                "organization": 1,
                "name": "Device 1",
                "description": "outsite temperature"
            },
            {
                "id": 1,
                "organization": 1,
                "name": "0234-7",
                "description": "outsite temperature"
            }
        ]
    ```


=== "401 Unauthorized"

    ```json
        {
            "detail": "Given token not valid for any token type",
            "code": "token_not_valid",
            "messages": [
                {
                    "token_class": "AccessToken",
                    "token_type": "access",
                    "message": "Token is invalid or expired"
                },
                {
                    "token_class": "DeviceToken",
                    "token_type": "device",
                    "message": "Token is invalid or expired"
                },
                {
                    "token_class": "AdminToken",
                    "token_type": "access",
                    "message": "Token is invalid or expired"
                }
            ],
            "status_code": 401,
            "message": "Something went wrong"
        }
    ```


## GET /api/devices/{device_id}/

Retourne les informations d'un device.

### Authentification

Bearer Token (JWT), que vous pouvez obtenir en vous authentifiant avec votre compte adresse email et votre admin token disponible dans l'interface web.

Pour obtenir plus d'information, voir la page : [Authentification](/docs/api/authentication.md)

### Path parameters

| Nom       | Requis |           Description           |
|-----------|:------:|:-------------------------------:|
| device_id |  oui   | L'identifiant unique du device. |

### Exemples de réponses

=== "200 OK"

    ```json
        {
            "id": 2,
            "organization": 1,
            "name": "Device 1",
            "description": "outsite temperature"
        }
    ```

=== "400 Bad Request"

    ```json
        {
            "detail": "Invalid device",
            "status_code": 400,
            "message": "Device does not exist or does not belong to you"
        }
    ```

=== "401 Unauthorized"
    
    ```json
        {
            "detail": "Given token not valid for any token type",
            "code": "token_not_valid",
            "messages": [
                {
                    "token_class": "AccessToken",
                    "token_type": "access",
                    "message": "Token is invalid or expired"
                },
                {
                    "token_class": "DeviceToken",
                    "token_type": "device",
                    "message": "Token is invalid or expired"
                },
                {
                    "token_class": "AdminToken",
                    "token_type": "access",
                    "message": "Token is invalid or expired"
                }
            ],
            "status_code": 401,
            "message": "Something went wrong"
        }
    ```

## POST /api/devices/

## PUT /api/devices/{device_id}/

## DELETE /api/devices/{device_id}/